swagger: "2.0"
info:
  version: "1.0.0"
  title: "Seleya-API"
  termsOfService: "http://www.seleyatech.com"
  contact:
    email: "it@seleyatech.com"
host: "www.seleyatech.com"
basePath: "/v1"
tags:
- name: "auth"
  description: "User authentication system"
schemes:
- "https"
- "http"
paths:
  /auth/login:
    post:
      tags:
      - "auth"
      summary: "User login"
      description: ""
      operationId: "oauth2_login"
      produces:
      - "application/json"
      parameters:
      - name: "username"
        in: "query"
        required: true
        type: "string"
      - name: "password"
        in: "query"
        required: true
        type: "string"
      responses:
        "200":
          description: "successful operation"
          schema:
            $ref: '#/definitions/OAuthLogin'
  /auth/oauth/token:
    post:
      tags:
      - "auth"
      summary: "Get token"
      description: ""
      operationId: "oauth2_token"
      produces:
      - "application/json"
      parameters:
      - name: "grant_type"
        in: "query"
        description: "OAuth2AuthorizationCode"
        required: true
        type: "string"
        default: "password"
      - name: "username"
        in: "query"
        description: "Login Username"
        required: true
        type: "string"
      - name: "password"
        in: "query"
        description: "Login Password"
        required: true
        type: "string"
      - name: "scope"
        in: "query"
        description: "scope type"
        required: true
        type: "string"
        default: "profile"
      - name: "code"
        in: "query"
        type: "string"
        description: "Used in the second step to call the oauth2/token interface to obtain the authorized access token."
      - name : "client_id"
        in: "query"
        description: "AppKey assigned during registration"
        required: true
        type: "string"
      - name : "client_secret"
        in: "query"
        description: "AppSecret assigned during registration"
        required: true
        type: "string"
      responses:
        "200":
          description: "successful operation"
          schema:
            $ref: '#/definitions/OAuthToken'
  /auth/oauth/refresh_token:
    get:
      tags:
      - "auth"
      summary: "Get token"
      description: ""
      produces:
      - "application/json"
      parameters:
      - name: "response_type"
        in: "query"
        required: true
        type: "string"
        default: "code"
      - name: "client_id"
        in: "query"
        required: true
        type: "string"
        description: "AppKey assigned during registration"
      - name: "scope"
        in: "query"
        required: true
        type: "string"
        default: "profile"
      responses:
        "200":
          description: "Successful get token"
          schema:
            type: "object"
            properties:
              code:
                type: "string"
                description: "Used in the second step to call the oauth2/token interface to obtain the authorized access token."
  /auth/create_user:
    post:
      tags:
      - "auth"
      summary: "create user"
      description: ""
      operationId: "oauth2_create_user"
      produces:
      - "application/json"
      parameters:
      - name: "username"
        in: "query"
        required: true
        type: "string"
      - name: "password"
        in: "query"
        required: true
        type: "string"
      responses:
        "200":
          description: "successful operation"
          schema:
            $ref: '#/definitions/OAuthCreateUser'
  /auth/create_client:
    post:
      tags:
      - "auth"
      summary: "create client"
      operationId: "oauth2_create_client"
      produces:
      - "application/json"
      parameters:
      - name: "uid"
        in: "query"
        required: true
        type: integer
      - name: "client_name"
        in: "query"
        required: true
        type: string
        default: "seleya"
      - name: "client_uri"
        in: "query"
        required: true
        type: string
      - name: "grant_type"
        in: "query"
        required: true
        type: string
        default: "authorization_code\r\npassword"
      - name: "redirect_uri"
        in: "query"
        required: true
        type: string
      - name: "response_type"
        in: "query"
        required: true
        type: string
        default: "code"
      - name: "scope"
        in: "query"
        required: true
        type: string
        default: "profile"
      - name: "token_endpoint_auth_method"
        in: "query"
        required: true
        type: string
        default: "token_endpoint_auth_method"
      responses:
        "200":
          description: "successful operation"
          schema:
            $ref: '#/definitions/OAuthInfoClient'
      
definitions:
  OAuthToken:
    type: "object"
    properties:
      access_token:
        type: "string"
        description: "The only ticket authorized by the user is used to call the open interface. It is also the only ticket for third-party applications to verify user login."
      expires_in:
        type: "integer"
        description: "The life cycle of access_token, in seconds."
      refresh_token:
        type: "string"
        description: "Refresh the unique ticket of access_token"
      scope:
        type: "string"
        description: "Parameters required to apply for scope permissions, multiple scope permissions can be applied for at once, separated by commas"
      token_type:
        type: "string"
  OAuthCreateUser:
    type: "object"
    properties:
        user:
          type: "object"
          properties:
            uid:
              type: "string"
            username:
              type: "string"
  OAuthLogin:
    type: "object"
    properties:
        clients:
          type: "array"
          items:
            $ref: "#/definitions/OAuthInfoClient"
        user:
          type: "object"
          properties:
            uid:
              type: "string"
            username:
              type: "string"
  OAuthInfoClient:
    type: "object"
    properties:
      client_name:
          type: "string"
          description: "client name or platform name"
      client_secret:
          type: "string"
          description: "AppSecret assigned during registration"
      client_secret_expires_at:
          type: "integer"
          description: "secret expires"
      uid:
          type: "integer"
      client_id:
        type: "string"
        description: "AppKey assigned during registration"
      client_id_issued_at:
        type: "integer"
        description: "Client  create unix time"
      client_metadata:
        type: "object"
        $ref: "#/definitions/OAutClientMetadata"
  OAutClientMetadata:
    type: "object"
    properties:
      client_name:
        type: "string"
        description: "client name or platform name"
      client_uri:
        type: "string"
        description: "callback  url"
      grant_types:
        type: "array"
        items:
          type: "string"
      redirect_uris:
        type: "array"
        items:
          type: "string"
      response_types:
        type: "array"
        items:
          type: "string"
      scope:
        type: "string"
      token_endpoint_auth_method:
        type: "string"
        description: "token  check auth_method"
        
    
          